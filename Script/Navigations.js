import { Navigation } from 'react-native-navigation';
var Constants=require('./Constants');
import Login from './LoginScreen';
import SecondScreen from './SecondScreen';
import Home from './HomeScreen';
import  RegisterScreen  from "./RegisterScreen";
import  SearchScreen  from "./SearchScreen";
import  SettingScreen  from "./SettingScreen";

Navigation.registerComponent(`LoginScreen`, () => Login);
Navigation.registerComponent(`SecondScreen`, () => SecondScreen);
Navigation.registerComponent(`HomeScreen`, () => Home);
Navigation.registerComponent('RegisterScreen', () => RegisterScreen);
Navigation.registerComponent('SearchScreen', () => SearchScreen);
Navigation.registerComponent('SettingScreen', () => SettingScreen);
//require('./Icons/search.png')


export const goToLogin = () => Navigation.setRoot({
  root: {

    stack: {
      id: 'stackMain',
      children: [
        {
          component: {
            name: "LoginScreen"
          }
        }
      ],
       options: {
        statusBar: {
        visible: true,
        drawBehind: false,
        backgroundColor:Constants.statusbarclr,
         },
     topBar: {
     visible: false,
     drawBehind: true,
     //animate: true,
      }
    }
    }

  }
});

const iconColor = '#444';
const selectedIconColor = '#ed2d4d';
const textColor = '#444';
const selectedTextColor = '#0089da';

export const goToTabs = () => Navigation.setRoot({
    
 

     root: {

    stack: {
      id: 'stackMain',
      children: [
        {
          component: {
            name: "HomeScreen"
          }
        }
      ],
       options: {
        statusBar: {
        visible: true,
        drawBehind: false,
        backgroundColor:Constants.statusbarclr,
         },
     topBar: {
     visible: false,
     drawBehind: true,
     //animate: true,
      }
    }
    }

  }
});



   // Navigation.setRoot({
   //    root: {

   //    bottomTabs: {
   //    id: 'BottomTabsId',
   //    children: [

   //    {
   //    stack: {
   //    id: 'tab0',
   //    children: [
   //    {
   //    component: {
   //            name: "HomeScreen",
   //            options: {
   //              bottomTab: {
   //                fontSize: 11,
   //                //text: 'Home',
   //                icon: require('./Icons/newsicon.png'),
   //                iconColor,
   //                selectedIconColor,
   //                selectedTextColor,
   //                iconInsets: { top: 8, bottom: 0 },
   //                titleDisplayMode:'alwaysHide' // Sets the title state for each tab

   //              }
   //            },
   //            passProps: {
   //              username
   //            },

   //          },
   //    },
   //    ],

   //    options: {
   //      statusBar: {
   //      visible: true,
   //      drawBehind: false,
   //      backgroundColor:Constants.statusbarclr,
   //       },
   //   topBar: {
   //   visible: false,
   //   drawBehind: true,
   //   //animate: true,
   //    }
   //  }
   //    }
   //    },


   //    {
   //    stack: {
   //    id: 'tab1',
   //    children: [
   //    {
   //    component: {
   //            name: "SearchScreen",
   //            options: {
   //              bottomTab: {
   //                fontSize: 11,
   //               // text: 'Search',
   //                icon: require('./Icons/pdficon.png'),
   //                iconColor,
   //                selectedIconColor,
   //                selectedTextColor,
   //                iconInsets: { top: 8, bottom: 0 },
   //              }
   //            }
   //          },
   //    },
   //    ],
   //    options: {
   //      statusBar: {
   //      visible: true,
   //      drawBehind: false,
   //      backgroundColor:Constants.statusbarclr,
   //       },
   //   topBar: {
   //   visible: false,
   //   drawBehind: true,
   //   //animate: true,
   //    }
   //  }
   //    }
   //    },



   //    {
   //    stack: {
   //    id: 'tab2',
   //    children: [
   //    {
   //    component: {
   //            name: "SettingScreen",
   //            options: {
   //              bottomTab: {
   //                fontSize: 11,
   //               // text: 'Settings',
   //                icon: require('./Icons/more.png'),
   //                iconColor,
   //                selectedIconColor,
   //                selectedTextColor,
   //                iconInsets: { top: 8, bottom: 0 },
   //              }
   //            }
   //          },
   //    },
   //    ],
   //    options: {
   //      statusBar: {
   //      visible: true,
   //      drawBehind: false,
   //      backgroundColor:Constants.statusbarclr,
   //       },
   //   topBar: {
   //   visible: false,
   //   drawBehind: true,
   //   //animate: true,
   //    }
   //  }
   //    }
   //    },


              
   //    ],
           

      
          
   //    }
   //    }
   //    });

//}
