
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,ImageBackground,Dimensions,
  Image,
  Platform,
  ActivityIndicator,
  ToastAndroid,
  NetInfo,
  Keyboard,
  SafeAreaView,
  ProgressBarAndroid
 } from 'react-native';
 import {Navigation} from 'react-native-navigation';
 import Mybutton from './components/Mybutton';
 import Mytext from './components/Mytext';
import Textinput from './components/Textinput';
 var Constants=require('./Constants');
//  import 'core-js/es6/map';
// import 'core-js/es6/symbol';
//import 'core-js/fn/symbol/iterator';
var Header=require('./Header');
var newspaper=require('./Icons/logo.png');
var checkboxUncheck=require('./Icons/checkboxUncheck.png');
var facebook=require('./Icons/facebok.png');
var google=require('./Icons/google.png');
var mail=require('./Icons/mail.png');

class RegisterScreen extends Component{


constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
    this.state={
             email: "",
             phoneNumber:'',
             address:'',
             names: '',
             password: "",
             passwordConfirm: "",
             visible:false,
           
             }   
    
          }



componentWillMount(){
 
}

signUp = () => {
   
  };

navigatetoHome(uId){
     
     Navigation.push(this.props.componentId, {
    component: {
    name: 'HomeScreen',
    //passProps:{data,compare},
    options: {
      animations: {
        pop: {
          enable: false
        }
      }
    }
  },

});
}

signInGoogle(){
  
}

signInFacebook(){
  
}



render(){
  
   
  if (this.state.loader) {
    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>
      <ActivityIndicator size="large" color="#961a36" alignItems='center' justifyContent='center'/>
      </View>
    );
  }

  return(
   

    <SafeAreaView style={{flex:1,backgroundColor:'white'}}>

         
         <ScrollView style={{flex: 0.9}}>
      

       <View style={{flex:0.1,marginTop:100,aligment:'center',alignItems:'center' ,justifyContent:'center'}}>

     
         <Text style={{color:'black',fontSize:20,textAlign:'center',margin:10}}>Sign up </Text>
      </View>



     


         <View style={{flex: 0.7, marginBottom: '10%', backgroundColor:'white',}}>
                 

                  <View style={{
            marginTop:5,flex:0.7,alignItems:'center',justifyContent:'center',
          }}>
          
                        <Textinput
                          placeholder="User Name"
                          value={this.state.names}
                          onChangeText={names => this.setState({ names })}
                          maxLength={30}
                          style={ styles.inputStyle }
                        />
          
                         
                        <Textinput
                          placeholder="Email"
                          value={this.state.phoneNumber}
                          onChangeText={phoneNumber => this.setState({ phoneNumber })}
                          maxLength={30}
                          keyboardType="numeric"
                          style={ styles.inputStyle }
                        /> 
           


                       <Textinput
                          placeholder="Password"
                          value={this.state.phoneNumber}
                          onChangeText={phoneNumber => this.setState({ address })}
                          maxLength={10}
                          style={ styles.inputStyle }
                        /> 
          
                       
                         <View style={{flex:0.1, width:'65%',marginTop:15,alignItems:'center', flexDirection:'row' }}>
                         
                           <TouchableOpacity style={{
                          justifyContent:'center',height:40,width:40,}}
                          onPress={()=> this.othetOption()}>
                           <Image
                            source= {checkboxUncheck}
                            style={{ width: 20, height: 20}}
                          />
                          
                          </TouchableOpacity>
                          
                          <View style={{height:40,justifyContent:'center'}}>
                          <Text style={{color:'black',fontSize:12,textAlign:'right',}}>I accept the Terms of Services </Text>
                          </View>

                         </View>


                        
          

          </View>


                <View style={{
              alignItems:'center',
              justifyContent:'center',flex:0.1,marginTop:10,
                      }}>
             <Mybutton
                title="Register"
                customClick={() => this.signUp()}
               />
            </View>

  

            </View>
            </ScrollView>

          </SafeAreaView>

   
      );  }
  }

  const styles = StyleSheet.create({
      container: {
        flex: 1,        
      },
      loginText:{
        color: Constants.ThemeColor,
        fontSize: 18,
        fontFamily: Constants.Family,
        textAlign: 'center',
        marginTop: '10%'
      },
      buttonDesign:{
  shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      margin:10,alignItems:'center',justifyContent:'flex-start',height:42,width:"65%",
      borderRadius:25, flexDirection:'row',backgroundColor:'white'
},
buttonDesignBlue:{
  shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      margin:10,alignItems:'center',justifyContent:'flex-start',height:42,width:"65%",
      borderRadius:25, flexDirection:'row',backgroundColor:'#3B5998'
},
      backgroundImage:{
        flex:1,
        alignSelf:'stretch',
      },
      textStyle: {
        marginBottom:5,
        //fontWeight: 'bold',
        fontSize:20,
        color:'white',
        alignItems:'center',justifyContent:'center'
      },
      inputStyle:{
        //backgroundColor: 'white',
      //  marginLeft: '15%',
        fontSize: 12,
      //  elevation: 2,
      //  shadowOpacity :10,
     //   marginRight:'15%',
       // fontFamily:Constants.Family,
       // borderRadius:25,
        height:40,
        width:"65%",
      //  borderColor: Constants.ThemeColor,
      //  borderWidth:1,
        //marginTop:10,
      //  paddingTop: 10,
       // paddingLeft:20,
      },
     
    });



module.exports=RegisterScreen;


