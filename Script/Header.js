const React = require('react');
const {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  Alert,
} = require('react-native');
import {Navigation} from 'react-native-navigation';

var Constants=require('./Constants');
var backArrow=require('./Icons/backArrow.png');
var filter=require('./Icons/profile.png');


     const Header = (props)=>{  

     navigateBack = () => {
        if (props.callBackFunction= null) {
          props.callBackFunction("data passed");
           }else{
             Navigation.pop(props.navigator);
          return false;
        }
      }
   	    

 return (
     <View style={{ height: 60,backgroundColor:'transparent',
     flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        

           {
         props.showBackbutton? (
         <View style={{flex:0.1,marginLeft:10}}>
          <TouchableOpacity style={styles.buttonDimension}
            onPress={navigateBack}>
            <Image style={styles.iconStyle} source={backArrow}/>
            </TouchableOpacity>
       </View>
           ):(
           <View style={{flex:0.1}}>
            
           </View>
            
             )
       }
            

      
       <View style={styles.textView}>
       <Text style={styles.titleStyle}> {props.title} </Text>
       </View>

       


     {props.showFilter? (

       <View style={{flex:0.1,marginRight:30,justifyContent:'center',}}>
           <TouchableOpacity style={styles.buttonDimension}
            onPress={() => props.filterSearch()}>
            <Image style={{height:35 ,width:35}} source={filter}/>
            </TouchableOpacity>
            </View>
          
       ):(

        <View style={{flex:0.1,}}>
        </View>
         
       )
       }
      



       </View>

    );



}





const styles=StyleSheet.create({

  headerStyle:{
  justifyContent:'center',
  width:window.width,
  },

  titleStyle:{
  textAlign:'center',
  color:  '#348AC7',  
  fontSize:18,
  fontWeight:'bold',
  },
  iconView:{
    flex:0.1,
    paddingRight:5,
  },
  textView:{
    flex:0.9,
    //paddingRight:15,
  //  marginTop:10,
    alignItems:'center',
    justifyContent:'center',
    // backgroundColor:'red',
  },
  notifyStyle:{
    flex:0.1,
    height:40,
  //  marginTop:10,
    alignItems:'flex-start',
    justifyContent:'center',
    // backgroundColor:'yellow',
  },
  iconStyle:{
    //31*46
    width:20,
     height:17,
  },
  buttonDimension:{
    alignItems:'center',
    justifyContent:'center',
    marginRight:8,
    width:40,
    height:40,
    // marginLeft:15,
    // backgroundColor:'gray',
  },
  iconMenuDimension:{
    width:25,
     height:20,

  },
  buttonNotificationDimention:{
    alignItems:'center',
    justifyContent:'center',
    marginRight:10,
    width:40,
    height:40,
  },
  iconNotifyDimension:{
     width:30,
     height:25,
     marginRight:15,
     resizeMode: 'contain',
  },
  notifyheaderStyle:{
  justifyContent:'center',
  alignItems:'center',
  height: 120,
  },



});

module.exports=Header;
