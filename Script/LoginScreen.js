
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
//  NetInfo,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Alert,
  ScrollView,
  SafeAreaView,
  Image,
  Platform,
  ActivityIndicator,
//  AsyncStorage,
  Dimensions,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
var {height, width} = Dimensions.get('window');
var {DEVICE_HEIGHT, DEVICE_WIDTH} = Dimensions.get('window');
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { goToTabs } from './Navigations';
var checkboxUncheck=require('./Icons/checkboxUncheck.png');
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
import Textinput from './components/Textinput';
var bgImage=require('./Icons/bg.png');
var logo=require('./Icons/logo.png');
var user=require('./Icons/user.png');
var lock=require('./Icons/lock.png');
class LoginScreen extends Component{
   
   static options(passProps) {
    return {

      topBar: {
        background: {
        color: '#a8f7c3',
        // component: {
        //      name: 'car.TopBar',
        //      aligment: 'center'
        // }
   },
        title: {
          text: 'Second Screen',
          color:'red',
        },
        // leftButtons: [
        //   {
        //     id: 'button',
        //     icon: require('./Icons/menux.png')
        //   }
        // ],
        // rightButtons: [{
        //   id: 'buttonOne',
        //   icon: require('./Icons/favorite.png')
        // }],
      }
    };
  }    

  constructor(props){

    super(props);
   
    Navigation.events().bindComponent(this);
   // this.props.navigator.setOnNavigatorEvent(this.addOnNavigationEvent.bind(this));
     
    this.state={
        bannerid:'',     
        bookArray:[],
        username:"",
        showOtherOption: false,
       
    }    
     
      



  }


 componentWillUnmount() {
  // NetInfo.isConnected.removeEventListener('change', this.handleConnectionChange);
   //console.log("unmount of home Screen");
    
}

 
componentDidMount(){
  
    

}



checkForInternectConnection(){
  


}

componentWillMount(){
  
   // NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
   //  NetInfo.isConnected.fetch().done(
   //    (isConnected) => {  }
   //  );
}

  


handleConnectionChange = (isConnected) => {



}  



  
login = async () => {
    const { username } = this.state;
    if (username) {
      await AsyncStorage.setItem('username', username);
      goToTabs(global.icons, username);
    }
  }
    
   
    
       rowSelected(){
    Navigation.push(this.props.componentId, {
  component: {
    name: 'SecondScreen',
   // passProps: {data},
    options: {
      animations: {
        pop: {
          enable: true
        }
      }
    }
  }
});
}


signUp(){

   Navigation.push(this.props.componentId, {
  component: {
    name: 'RegisterScreen',
   // passProps: {data},
    options: {
      animations: {
        pop: {
          enable: true
        }
      }
    }
  }
});
}

skip(){


   if(this.state.name != "" && this.state.Password != ""){

   const username =  AsyncStorage.getItem('username');

            // if (!username) {
            //   goToTabs(global.icons, username);
            // }else{
               this.loginFuc();
            // }
  }else{
   Alert.alert("Username or Password not valid!")
  }
}

loginFuc(){

   Navigation.push(this.props.componentId, {
  component: {
    name: 'SecondScreen',
   // passProps: {data},
    options: {
      animations: {
        pop: {
          enable: true
        }
      }
    }
  }
});

//   axios.post('http://epos.demo.techno-genes.com/api/user/login', {
//     username: 'awaisi77',
//     password: 'awaisi77'
//   })
//   .then(function (response) {
//     console.log(response.data);
//     if (response.data.status) {
      

//      const storeData = async (value) => {
//   try {
//     const jsonValue = JSON.stringify(response.data.user)
//     await AsyncStorage.setItem('@userInfo', jsonValue)
//   } catch (e) {
//     // saving error
//   }
// }


//       Alert.alert(response.data.message);
//     }else{
//       Alert.alert(response.data.message);
//     }
//   })
//   .catch(function (error) {
//     console.log(error);
//   });
}



forgetPassword(){
  Alert.alert("under development!")
}
   
  
render(){
  
   
  if (this.state.loader) {
    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center',}}>
      <ActivityIndicator size="large" color="#961a36" alignItems='center' justifyContent='center'/>
      </View>
    );
  }

  return(
   
     <ImageBackground source={bgImage} style={{flex: 1, resizeMode: "cover", justifyContent: "center"}}>
    <SafeAreaView style={{flex:1,}}>
   
         
        


     


         <View style={{flex: 0.95, marginBottom: '10%',alignItems:'center',justifyContent:'center'}}>
         
         <Image style={{height:130,width:130,marginTop:0,marginBottom:10}} source={logo} />
                 
           <Text style={{marginBottom:20,color:'white',fontSize:20,textAlign:'center',margin:10}}>Sign In </Text>

          <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center' }}>
          
          <View style={{height:100,borderBottomLefttRadius:7,borderTopLefttRadius:7,width:'12%',backgroundColor:"#113562",alignItems:'center',justifyContent:'center'}}>
          <View style={{flex:0.5,alignItems:'center',justifyContent:'center'}}>
          <Image style={{height:15,width:15,resizeMode:'contain'}} source={user} />
          </View>

          <View style={{flex:0.5,alignItems:'center',justifyContent:'center'}}>
          <Image style={{height:15,width:15, resizeMode:'contain'}} source={lock} />
          </View>
          </View>


          <View style={{height:100,borderBottomRightRadius:7,width:'65%',backgroundColor:'white',
          borderTopRightRadius:7,alignItems:'center',justifyContent:'center'}}>
                        <TextInput style = {styles.inputStyle}
               underlineColorAndroid = "transparent"
               placeholder = "Enter User Name"
               placeholderTextColor = "#113562"
               autoCapitalize = "none"
               onChangeText={name => this.setState({ name })}/>
          
                         
                        
           <View style={{height:1, backgroundColor:"#113562"}}>
           </View>


                     <TextInput style = {styles.inputStyle}
               underlineColorAndroid = "transparent"
               placeholder = "Enter Password"
               placeholderTextColor = "#113562"
               autoCapitalize = "none"
               secureTextEntry={true}
               onChangeText={Password => this.setState({ Password })}/>


               </View>
               </View>


              
             <Mybutton
                title="Sign in"
                customClick={() => this.skip()}
               />







                <TouchableOpacity
              style={{ ...styles.openButton, backgroundColor: "transparent",}}
              onPress={()=>this.forgetPassword()}
            >
              <Text style={{color:'white',fontSize:14}}>Forgot Password?</Text>
             
            </TouchableOpacity>



             <TouchableOpacity
              style={{ ...styles.signupButton, backgroundColor: "#ffffff",}}
              onPress={()=>this.forgetPassword()}
            >
              <Text style={{color:'grey',marginTop:5,fontSize:12}}>No Account yet? create one</Text>
             
            </TouchableOpacity>
            

  

            </View>
           
          
          </SafeAreaView>
          </ImageBackground>

   
      );  }
  }

const styles=StyleSheet.create({
outerContainer:{
  flex:1,
  backgroundColor:'#e0ebeb',
},
buttonDesignBlue:{
  shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      margin:8,alignItems:'center',justifyContent:'flex-start',height:42,width:"65%",
      borderRadius:25, flexDirection:'row',backgroundColor:'#3B5998'
},
inputStyle:{     
       //borderRadius:7,
      // borderWidth:1,
      // borderColor:"#ffffff",
       backgroundColor:"white",
       padding:10,
       margin:1,
        fontSize: 12,
        height:45,
        width:"65%",
     
      },
    openButton: {
   // backgroundColor: "white",
   // flexDirection:'row',
    alignItems:'center',
   // justifyContent:'space-between',
    borderRadius: 7,
    height:45,
    width:200,
    padding: 10,
    margin:5,
  },

  signupButton: {
    backgroundColor: "white",
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 15,
    height:40,
    //width:400,
    width:wp('65%'),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },


})

module.exports=LoginScreen;



























