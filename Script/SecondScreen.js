
import React, { Component, Fragment } from 'react';
import {
  AppRegistry,
  StyleSheet,
  //NetInfo,
  Text,
  ImageBackground,
  View,
  TextInput,
  TouchableOpacity,
  Alert,Modal,
  ScrollView,
  SafeAreaView,
  Textinput,
  Image,
  Platform,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  FlatList,
  TouchableHighlight,
  Picker,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
var {height, width} = Dimensions.get('window');
var bgImage=require('./Icons/bg.png');
import SearchableDropdown from 'react-native-searchable-dropdown';
import DatePicker from 'react-native-datepicker'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
var Header=require('./Header');
import axios from 'axios';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
var bgImage=require('./Icons/bgHome.png');
var logo=require('./Icons/profile.png');
var customerPic=require('./Icons/customer.png');
var productPic=require('./Icons/product.png');
var chargerPic=require('./Icons/freightCharger.png');
var logo=require('./Icons/logo3d.png');

var {DEVICE_HEIGHT, DEVICE_WIDTH} = Dimensions.get('window');
var AdCount=0;

const items = [
  // this is the parent or 'item'
  {
    //name: 'Products',
    //id: 0,
    // these are the children or 'sub items'
    children: [
      {
        name: 'Apple',
        rate:120,
        id: 0,
      },
      {
        name: 'Strawberry',
        rate:100,
        id: 1,
      },
      {
        name: 'Pineapple',
        rate:50,
        id: 2,
      },
      {
        name: 'Banana',
        rate:70,
        id: 3,
      },
      {
        name: 'Watermelon',
        rate:25,
        id: 4,
      },
      {
        name: 'Kiwi fruit',
        rate:150,
        id: 5,
      },
    ],
  },
  ];


class SecondScreen extends Component{
   
   static options(passProps) {
    return {

      topBar: {
        background: {
        color: 'transparent',
        // component: {
        //      name: 'car.TopBar',
        //      aligment: 'center'
        // }
   },
        title: {
          text: 'Sales',
          color:'black',
        },
        // leftButtons: [
        //   {
        //     id: 'button',
        //     icon: require('./Icons/menux.png')
        //   }
        // ],
        // rightButtons: [{
        //   id: 'buttonOne',
        //   icon: require('./Icons/favorite.png')
        // }],
      }
    };
  }    

  constructor(props){

    super(props);
   
    Navigation.events().bindComponent(this);
   // this.props.navigator.setOnNavigatorEvent(this.addOnNavigationEvent.bind(this));
     
    this.state={
        bannerid:'',
        dateSale:'',
        height:0,
        show:false,
        modalVisible:false,
        updateQuantity:'',
        updateRate:'',
        updateSaleTax:'',
        selectedProductName:'',
        selectedIndex:0,
         customers : [
                  {
                    children:[
                    {
                      id: 0,
                      name: 'Ali',
                    },
                    {
                      id: 1,
                      name: 'Fahad',
                    },
                    {
                      id: 2,
                      name: 'Nouman',
                    },
                    {
                      id: 3,
                      name: 'Awais',
                    },
                    {
                      id: 4,
                      name: 'Badar',
                    },
                    ],
                  },
                  ],
       selectedCustomers:"",
       selectedItem: [],
       selectedProducts: [],
       orderBy:"",
       freightCharges : [
                    {
                      children:[

                    {
                      id: 1,
                      name: 'Buyer',
                    },
                    {
                      id: 2,
                      name: 'Saler',
                    },
                    
                    ],
                  },
                  ],
       selectedFreight:"",
       
    }    
     
      



  }

  
 hideSideMenu() {
  Navigation.mergeOptions(this.props.componentId, {
    sideMenu: {
      left: {
        visible: true
      }
    }
  });
}

 

 componentWillUnmount() {
 //  NetInfo.isConnected.removeEventListener('change', this.handleConnectionChange);
   //console.log("unmount of home Screen");
    
}

 
componentDidMount(){
  
   // this.getProducts();
    this.getCustomers();

}

getProducts(){

  axios.get(`http://epos.demo.techno-genes.com/api/products`)
      .then(res => {
        console.log("products==", res.data);
        const nameList = res.data;
       // this.setState({ nameList });
      })

}

getCustomers(){

  axios.get(`http://epos.demo.techno-genes.com/api/contacts`)
      .then(res => {
        console.log("contacts= ", res.data.data);
        const nameList = res.data.data;
        this.setState({ customers : nameList });
      })

}

checkForInternectConnection(){
  


}

componentWillMount(){
  
   // NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
   //  NetInfo.isConnected.fetch().done(
   //    (isConnected) => {  }
   //  );
}

  


handleConnectionChange = (isConnected) => {



}  

 
       rowSelected(){
    
//    Navigation.push(this.props.componentId, {
//   component: {
//     name: 'SecondScreen',
//     passProps: {data},
//     options: {
//       animations: {
//         pop: {
//           enable: true
//         }
//       }
//     }
//   }
// });
}

  onValidate(){

  }

   modalToggle(value){
       this.setState({modalVisible:value});
  }

  onSelectedItemsChange = (selectedItem) => {
   
    console.log("item==",this.state.selectedItem, selectedItem);
     this.setState({ selectedItem });
     var product ={name:items[0].children[selectedItem].name, id:items[0].children[selectedItem].id, rate:items[0].children[selectedItem].rate, quantity:1,sTax:17}
     this.state.selectedProducts.push(product);
     this.setState({});
  };


  onProductChange(item){
     this.setState({modalVisible:true});
     this.setState({updateQuantity:item.quantity, updateRate:item.rate,
      updateSaleTax:item.sTax, selectedProductName:item.name, selectedIndex:item.id });
     
  }

  onProductUpdate(){
         let object = this.state.selectedProducts[this.state.selectedIndex];
         console.log("index",this.state.selectedIndex ,this.state.selectedProducts[this.state.selectedIndex]);
         var product ={name:object.name, rate:this.state.updateRate, id:this.state.selectedIndex, quantity:this.state.updateQuantity, sTax:this.state.updateSaleTax};
         this.state.selectedProducts[this.state.selectedIndex] = product ;
         this.setState({modalVisible:false});
  }

  onSelectCustomer = (selectedItem) => {
   this.setState({ selectedCustomers : selectedItem });
  }

  onSelectFreightCharges = (selectedItem) => {
   this.setState({ selectedFreight : selectedItem});
  }

  saveOrder(){
    Alert.alert("Order Saved!")
  }

   
  render(){


   

    return(
     <ImageBackground source={bgImage} style={{flex: 1, resizeMode: "cover", justifyContent: "center"}}>
      <SafeAreaView style={{flex:1}}>
      <View style={{flex:1,}}>
     <Header navigator={this.props.componentId} colorBackground={true} filterSearch={this.applyFilter} showFilter={false} showBackbutton={true}  title='Add Sales'/>
     <ScrollView style={{flex:1}}>
    {/*}  <Text style={{marginLeft:20,textAlign:'left',marginTop:50,fontSize:25}}> Add Sales: </Text>*/}
      
     <View style={{flex:0.07,marginTop:0,flexDirection:'row'}}>

     <View style={{flex:0.65,marginTop:0,flexDirection:'row'}}>
      <View style={{flex:0.55,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#336699",textAlign:'center',fontSize:16}}>Serial Number: </Text>
      </View>

       <View style={{flex:0.45,alignItems:'flex-start',justifyContent:'center',}}>
       <View style={{height:25,borderRadius:30,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
      <Text style={{marginLeft:5,marginRight:5,textAlign:'center',fontSize:14}}> OR1202 </Text>
      </View>
      </View>
      </View>

      <View style={{flex:0.35,justifyContent:'center',alignItems:'center'}}>
      <Text style={{marginLeft:5,textAlign:'center',fontSize:14}}> 12 June,2020 </Text>
      </View>
     </View>



     {/* <View style={{flex:0.07,marginTop:0,flexDirection:'row'}}>
      <View style={{flex:0.5,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#348AC7",textAlign:'left',fontSize:16}}>Date: </Text>
      </View> 

       <View style={{flex:0.5,justifyContent:'center'}}>
      <Text style={{marginLeft:5,textAlign:'center',fontSize:14}}> 20 May,2020 </Text>
      </View>
     </View>*/}


        <View style={{}}>
         <View style={{flex:0.1,marginTop:20}}>

       <View style={{flexDirection:"row",}}>
       <Image style={{height:25,width:21,marginLeft:25}} source={customerPic} />
      <Text style={{marginLeft:5,color:"#336699",textAlign:'left',fontSize:16}}> Select Customer: </Text>
      </View>

          <View style={{justifyContent:'center',marginTop:10,backgroundColor:'white',height:45,borderRadius:15,marginRight:25,marginLeft:25}}>
          <SectionedMultiSelect
          items={this.state.customers}
          uniqueKey="id"
          subKey="children"
          selectText="Choose Customer..."
          showDropDowns={true}
          single  = {true}
          expandDropDowns ={true}
          readOnlyHeadings={true}
          onSelectedItemsChange={this.onSelectCustomer}
          selectedItems={this.state.selectedCustomers}
        />
        </View>

       
        </View>
       </View>


       <View style={{flex:0.07,marginTop:10,flexDirection:'row'}}>

      <View style={{flex:0.5,flexDirection:'row'}}>
       <View style={{flex:0.4,justifyContent:'center',alignItems:"flex-end"}}>
      <Text style={{marginLeft:10,fontFamily:'Arial',color:"#336699",textAlign:'left',fontSize:14}}>LIMIT: </Text>
      </View>
       <View style={{flex:0.6,justifyContent:'center'}}>
      <Text style={{marginLeft:10,textAlign:'left',fontSize:12}}>100000</Text>
      </View>

      </View>

      <View style={{flex:0.5,flexDirection:'row'}}>
       <View style={{flex:0.5,justifyContent:'center',alignItems:'flex-end'}}>
      <Text style={{marginLeft:10,color:"#336699",textAlign:'left',fontSize:14}}>USED: </Text>
      </View>
       <View style={{flex:0.5,justifyContent:'center'}}>
      <Text style={{marginLeft:10,textAlign:'left',fontSize:12}}>50000 </Text>
      </View>

      </View>
     </View>


      <View style={{}}>
      <View style={{flex:0.07,marginTop:0,flexDirection:'row'}}>
      <View style={{flex:0.28,justifyContent:'center',alignItems:'flex-end'}}>
      <Text style={{marginLeft:15,color:"#666666",textAlign:'left',fontSize:12}}>ORDER BY: </Text>
      </View>

       <View style={{flex:0.7,justifyContent:'center'}}>
      <TextInput style = {styles.inputStyle}
               underlineColorAndroid = "transparent"
               placeholder = "Enter Name"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onChangeText={orderBy => this.setState({ orderBy })}/>
      </View>
     </View>

      <View style={{height:0.5,marginTop:25,marginLeft:'25%',width:"50%",backgroundColor:'grey',alignItems:'center',justifyContent:'center'}}>
     </View>
     </View>


    


     <View style={{flex:0.07,marginTop:10,flexDirection:'row'}}>

      <View style={{flex:0.45,flexDirection:'row'}}>
       <View style={{flex:0.2,justifyContent:'center'}}>
      <Text style={{marginLeft:10,fontFamily:"Arial",color:"#336699",textAlign:'left',fontSize:13}}>Po No:</Text>
      </View>
       <View style={{flex:0.8,justifyContent:'center'}}>
      <TextInput style = {[styles.inputStyle,{height:35}]}
               underlineColorAndroid = "transparent"
               placeholder = "Enter PO Number"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onChangeText={orderBy => this.setState({ orderBy })}/>
      </View>

      </View>

      <View style={{flex:0.55,flexDirection:'row'}}>
       <View style={{flex:0.25,justifyContent:'center'}}>
      <Text style={{marginLeft:5,color:"#336699",fontFamily:"Arial",textAlign:'left',fontSize:13}}>Po Date:</Text>
      </View>
       <View style={{flex:0.75,justifyContent:'center'}}>
      
      <DatePicker
            style={{ marginTop: hp('0.1%'), padding:2, width: wp('38%') }}
            date={this.state.dateSale}
            mode="date"
            placeholder="Select Date"
            format="Do MMMM[,] YYYY"
            minDate="01st JAN 2020"
            maxDate="31st DEC 2020"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            onCloseModal={() => this.onValidate()}
            customStyles={{
              dateInput: {
                backgroundColor:"white",     
                borderRadius:21,
                //marginTop: 8,
                //borderColor: '#348AC7',
                //borderWidth: 0.5,
                //borderRadius: 7,
                textAlign:'center',
                height: 35,// color: 'black'
              },
              placeholderText:{
                color: '#a3a3a3',
                fontSize: 14,
                alignSelf: 'center',
                marginLeft: wp('2.9%'),
              }, dateText: { alignSelf: 'center', marginLeft: wp('2.9%') }
            }}
            onDateChange={(date) => {this.setState({dateSale: date})}}
          />
      </View>

      </View>
     </View> 



       <Fragment>
        <View style={{flex:0.1,marginTop:20}}>

         <View style={{flexDirection:"row",}}>
       <Image style={{height:25,width:25,marginLeft:25}} source={productPic} />
      <Text style={{marginLeft:5,color:"#336699",textAlign:'left',fontSize:16}}> Select Product: </Text>
      </View>

       <View style={{justifyContent:'center',marginTop:10,backgroundColor:'white',height:45,borderRadius:15,marginRight:25,marginLeft:25}}>
       <SectionedMultiSelect
          items={items}
          uniqueKey="id"
          subKey="children"
          selectText="Choose Products..."
          showDropDowns={true}
          single  = {true}
          expandDropDowns ={true}
          readOnlyHeadings={true}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={this.state.selectedItem}
        />
        </View>

        </View>

       {/*  <View style={{height:0.5,marginTop:25,marginLeft:'25%',width:"50%",backgroundColor:'grey',alignItems:'center',justifyContent:'center'}}>
     </View> */}
        </Fragment>

        



       {this.state.selectedProducts.length > 0 ?
       <View style={{flex:1,marginTop:20,alignItems:'center',justifyContent:'center'}}>
       <View style={{width:wp('92%'),backgroundColor:'white',borderRadius:21}}>
       <Text style={{marginLeft:5,marginTop:6,textAlign:'center',fontSize:14}}> View more details </Text>
        <View style={{
              marginTop:10,
              flexDirection:'row',
            }}>
                <View style={{
                  flex:1.1,
                  alignItems:'center',
                  justifyContent:'center',

                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>Item</Text>
                </View>

                <View style={{
                  flex:1,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>Quantity</Text>
                </View>

                <View style={{
                  flex:0.9,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>Rate</Text>
                </View>

                <View style={{
                  flex:1,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>Gross Amount</Text>
                </View>


                <View style={{
                  flex:1,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>Sales Tax</Text>
                </View>

                <View style={{
                  flex:1,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>S.tax Amount</Text>
                </View>

                <View style={{
                  flex:1,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'300',color:'#666666'}}>Gross Total</Text>
                </View>

            </View>


        
         <FlatList
      style={{height:170,width:wp('92%'),}}
      keyExtractor={(item, index) => ''+item.id}
      data={this.state.selectedProducts}
      extraData={this.state}
      renderItem={({item,index}) =>
      <TouchableOpacity onPress={()=>this.onProductChange(item) } style={{
        marginRight:5,
        marginLeft:5,
        marginTop:1,
       // marginBottom:index==this.state.inningList.length-1 ? 10:0,
        backgroundColor:'white',
      }}>
        <View style={{
          alignItems:'center',justifyContent:'center'
        }}>



            <View style={{
              borderWidth:0.3,
              marginTop:10,
              flexDirection:'row',
            }}>
                <View style={{
                  flex:1.3,
                  borderRightWidth:0.3,
                  alignItems:'center',
                  justifyContent:'center',

                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>{item.name}</Text>
                </View>

                <View style={{
                  flex:0.85,
                  borderRightWidth:0.3,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>{item.quantity}</Text>
                </View>

                <View style={{
                  flex:0.85,
                  borderRightWidth:0.3,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>{item.rate}</Text>
                </View>

                <View style={{
                  flex:1,

                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>{item.quantity * item.rate}</Text>
                </View>


                <View style={{
                  flex:1,
                  borderRightWidth:0.3,
                  borderLeftWidth:0.3,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>{item.sTax}</Text>
                </View>

                <View style={{
                  flex:1,
                  borderRightWidth:0.3,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>12</Text>
                </View>

                <View style={{
                  flex:1,
                  alignItems:'center',
                  justifyContent:'center',
                }}>
                <Text style={{fontWeight:'400',color:'#4180BF'}}>{(item.quantity * item.rate) + item.sTax} </Text>
                </View>

            </View>



        </View>
      </TouchableOpacity>

          }
      />

      </View>
      </View>

      :
      <View style={{height:25,alignItems:'center',justifyContent:'center'}}>
      {/*<Text style={{textAlign:'center'}}> No Item Selected </Text>*/}
      </View>
    }




        {/* <View style={{flex:0.1,marginTop:20}}>
         <Text style={{marginLeft:5,textAlign:'left',fontSize:16}}> Select Date: </Text>
          <DatePicker
            style={{ marginTop: hp('0.1%'), padding:5, width: wp('100%') }}
            date={this.state.dateSale}
            mode="date"
            placeholder="Date of Birth"
            format="Do MMMM[,] YYYY"
            minDate="01st JAN 1950"
            maxDate="31st DEC 2000"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            onCloseModal={() => this.onValidate()}
            customStyles={{
              dateInput: {
                
                //marginTop: 8,
                borderColor: '#348AC7',
                borderWidth: 0.8,
                borderRadius: 5,
                height: hp('5.0%'), color: 'black'
              },
              placeholderText:{
                color: '#888',
                fontSize: 14,
                alignSelf: 'flex-start',
                marginLeft: wp('2.9%'),
              }, dateText: { alignSelf: 'flex-start', marginLeft: wp('2.9%') }
            }}
            onDateChange={(date) => {this.setState({dateSale: date})}}
          />
        </View>*/}


         <Fragment>
         <View style={{flex:0.1,marginTop:10}}>

           <View style={{flexDirection:"row",}}>
       <Image style={{height:25,width:25,marginLeft:25}} source={chargerPic} />
      <Text style={{marginLeft:5,color:"#336699",textAlign:'left',fontSize:16}}> Freight Charges: </Text>
      </View>
          
          <View style={{justifyContent:'center',marginTop:10,backgroundColor:'white',height:45,borderRadius:15,marginRight:25,marginLeft:25}}>
          <SectionedMultiSelect
          items={this.state.freightCharges}
          uniqueKey="id"
          subKey="children"
          selectText="Select Payer..."
          showDropDowns={true}
          single  = {true}
          expandDropDowns ={true}
          readOnlyHeadings={true}
          onSelectedItemsChange={this.onSelectFreightCharges}
          selectedItems={this.state.selectedFreight}
        />
        </View>
       
        </View>

         <View style={{height:0.5,marginTop:25,marginLeft:'25%',width:"50%",backgroundColor:'grey',alignItems:'center',justifyContent:'center'}}>
     </View>
       </Fragment>




        <View style={{flex:0.1, marginTop: 20, alignItems:'center'}}>

        <TextInput style = {[styles.largeInput, {height: Math.max(40, this.state.height)}]}
               multiline={false}
               maxLength={300}
               underlineColorAndroid = "transparent"
               placeholder = "Enter Delivery instructions"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onContentSizeChange={(event) => {
            this.setState({ height: event.nativeEvent.contentSize.height })}}
               onChangeText={orderBy => this.setState({ orderBy })}/>

        </View>


        <View style={{flex:0.1, marginTop: 20, alignItems:'center'}}>

        <TextInput style = {[styles.largeInput, {height: Math.max(40, this.state.height)}]}
               multiline={false}
               maxLength={300}
               underlineColorAndroid = "transparent"
               placeholder = "Enter Delivery Schedule"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onContentSizeChange={(event) => {
            this.setState({ height: event.nativeEvent.contentSize.height })}}
               onChangeText={orderBy => this.setState({ orderBy })}/>

        </View>


        <View style={{flex:0.1, marginTop: 20, marginBottom:40, alignItems:'center'}}>

        <TextInput style = {[styles.largeInput, {height: Math.max(40, this.state.height)}]}
               multiline={false}
               maxLength={500}
               underlineColorAndroid = "transparent"
               placeholder = "Enter Remarks"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onContentSizeChange={(event) => {
            this.setState({ height: event.nativeEvent.contentSize.height })}}
               onChangeText={orderBy => this.setState({ orderBy })}/>

        </View>




            <View style={{flex:0.,marginTop:5, justifyContent:'center', alignItems:'center'}}>
      <TouchableOpacity
              style={{ ...styles.openButton, backgroundColor: "#113562",}}
              onPress={()=>this.saveOrder()}
            >
              <Text style={{color:'white',fontSize:18}}>Save Order</Text>
             
            </TouchableOpacity>
            </View>







         <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >

       <View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'rgba(0,0,0,0.5)',height:"90%"}}>
            <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center'}} 
            >


            <View style={{ backgroundColor:'white',borderRadius:15,height:490,width:wp('92%')}}>

            <View style={{alignItems:'center'}}>
            <Image style={{height:80,width:80,marginTop:-40}} source={logo} />
            </View>

          <View style={styles.modalView}>
            <Text style={{color:"#113562",color:"#113562",fontSize:24,fontFamily:"Arial"}}>Edit Product!</Text>
           </View>
            

            <View style={{flex:0.1,marginTop:10,flexDirection:'row'}}>
      <View style={{flex:0.5,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#348AC7",textAlign:'center',fontSize:16}}>Product Name: </Text>
      </View>

       <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}>
      <Text style={{marginLeft:1,color:"#3F7DBC",textAlign:'center',fontSize:18}}> {this.state.selectedProductName} </Text>
      </View>
     </View>

            
     <View style={{flex:0.2,marginTop:10,flexDirection:'row'}}>
        
      <View style={{flex:0.4,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#348AC7",textAlign:'center',fontSize:16}}>Quantity: </Text>
      </View>

      <View style={{flex:0.6}}>
      {/* <View style={{flex:0.1,justifyContent:'center'}}>
        <TouchableOpacity
              style={{marginLeft:10, backgroundColor: "white" }}
              onPress={()=>this.modalToggle(!this.state.modalVisible)}
            >
              <Text style={{fontSize:28,color:"#09318c"}}>+</Text>
              
            </TouchableOpacity>
      </View>*/}

       <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
      <TextInput style = {styles.inputStyleModal}
               underlineColorAndroid = "transparent"
               placeholder = "Enter desire quantity"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onChangeText={updateQuantity => this.setState({ updateQuantity })}
               defaultValue={this.state.updateQuantity}
               />
      </View>

     {/* <View style={{flex:0.15,alignItems:'flex-start',justifyContent:'center'}}>
       <TouchableOpacity
              style={{marginRight:10, backgroundColor: "white" }}
              onPress={()=>this.modalToggle(!this.state.modalVisible)}
            >
              <Text style={{fontSize:30,color:"red",textAlign:'left'}}>-</Text>
              
            </TouchableOpacity>
      </View>*/}

      </View>
      </View>





      <View style={{flex:0.2,marginTop:10,flexDirection:'row'}}>
      <View style={{flex:0.4,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#348AC7",textAlign:'center',fontSize:16}}>Rate: </Text>
      </View>

      <View style={{flex:0.6,justifyContent:'center',alignItems:'center'}}>
      <TextInput style = {styles.inputStyleModal}
               underlineColorAndroid = "transparent"
               placeholder = "Enter unit rate"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onChangeText={updateRate => this.setState({ updateRate })}
               value={this.state.updateRate}/>
      </View>
      
      </View>





      <View style={{flex:0.2,marginTop:10,flexDirection:'row'}}>
      <View style={{flex:0.4,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#348AC7",textAlign:'center',fontSize:16}}>Sales Tax: </Text>
      </View>

      <View style={{flex:0.6,justifyContent:'center',alignItems:'center'}}>
      <TextInput style = {styles.inputStyleModal}
               underlineColorAndroid = "transparent"
               placeholder = "Sales tax %"
               placeholderTextColor = "#a3a3a3"
               autoCapitalize = "none"
               onChangeText={updateSaleTax => this.setState({ updateSaleTax })}
               value={this.state.updateSaleTax}/>
      </View>
      
      </View>



    {/* <View style={{flex:0.1,marginTop:0,flexDirection:'row'}}>
      <View style={{flex:0.5,justifyContent:'center'}}>
      <Text style={{marginLeft:10,color:"#348AC7",textAlign:'left',fontSize:16}}>Product Name: </Text>
      </View>

       <View style={{flex:0.5,justifyContent:'center'}}>
      <Text style={{marginLeft:5,textAlign:'center',fontSize:14}}> NN1232 </Text>
      </View>
     </View>*/}


      <View style={{flex:0.2,marginTop:15, justifyContent:'center', alignItems:'center'}}>
      <TouchableOpacity
              style={{ ...styles.openButton, backgroundColor: "#113562",}}
              onPress={()=>this.onProductUpdate()}
            >
              <Text style={{color:'white',fontSize:18}}>Update Product</Text>
             
            </TouchableOpacity>
            </View> 

            <View style={{flex:0.,marginTop:5, justifyContent:'center', alignItems:'center'}}>
      <TouchableOpacity
              style={{ ...styles.openButton, backgroundColor: "#e33c09",}}
              onPress={()=>this.modalToggle(!this.state.modalVisible)}
            >
              <Text style={{color:'white',fontSize:18}}>Cancel</Text>
             
            </TouchableOpacity>
            </View>




          
       



        </View>
         </TouchableOpacity>
        </View>
      </Modal>


      
      </ScrollView>
      </View>
      </SafeAreaView>
      </ImageBackground>

    );


  }


}

const styles=StyleSheet.create({
outerContainer:{
  flex:1,
  backgroundColor:'#e0ebeb', //Constants.ThemeColor,
},
inputStyle:{     
       borderRadius:28,
      // borderWidth:0.5,
     //  borderColor:"#348AC7",
     backgroundColor:'white',
     textAlign:'center',
       padding:10,
       margin:10,
        fontSize: 12,
        height:40,
        width:"85%",
     }, 
largeInput:{ 
       backgroundColor:"white",     
       borderRadius:21,
       // fontSize: 12,
       // height:40,
       // maxHeight:80,
        width:wp('65%'),
        textAlign:'center'
     },
      centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    
  },
  modalView: {
    margin: 5,
    backgroundColor: "white",
   // borderRadius: 20,
    padding: 15,
    alignItems: "center",
  },
  inputStyleModal:{     
       borderRadius:12,
       //borderWidth:0.5,
       //borderColor:"#348AC7",
       padding:10,
       backgroundColor:"#F7F7F7",
       textAlign:'center',
       margin:10,
       color:"#3F7DBC",
        fontSize: 18,
        height:40,
        width:wp("40%"),
     },
     openButton: {
   // backgroundColor: "white",
   // flexDirection:'row',
    alignItems:'center',
   // justifyContent:'space-between',
    borderRadius: 21,
    height:45,
    width:200,
    padding: 10,
    margin:8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },

})

module.exports=SecondScreen;



























