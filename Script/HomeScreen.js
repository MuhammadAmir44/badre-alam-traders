import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Alert,
  View,
  FlatList,
  Platform,
  TouchableOpacity,
  Image,
  TextInput,
  SafeAreaView,
  ImageBackground,
  ScrollView,
  Dimensions
} from 'react-native';
var Constants=require('./Constants');
const {height, width} = Dimensions.get('window');
import {Navigation} from 'react-native-navigation';
var bgImage=require('./Icons/bgHome.png');
var logo=require('./Icons/logo3d.png');
var Header=require('./Header');

class HomeScreen extends Component{
  static options(passProps) {
     return {

       statusBar: {
    visible: true,
    style: 'dark',
    backgroundColor:'#fafcfb',
      },
    }
  }
  constructor(props){
    super(props);
      Navigation.events().bindComponent(this);
    this.state={
      showProgress:false,
      refreshing: true,
       resultArray:[],originalArray:[],
          finalArray:[
   { title: 'Sales', data: 'Payments', image: require('./Icons/sales.png'),index:1},
   { title: 'Payments', data: 'Karachi',image: require('./Icons/payments.png'), index:2},
   { title: 'Out standing reports', data: 'Islamabad', image: require('./Icons/outStandings.png'),index:3},
   { title: 'Progress', data: 'Quetta', image: require('./Icons/progress.png'), index:4},
   { title: 'Chat', data: 'Peshawar',image: require('./Icons/chat.png'),index:5},
   { title: 'Stock', data: 'Peshawar',image: require('./Icons/stocks.png'), index:6},
   ],
     
    }

  }
          



componentWillUnmount() {

}


componentDidMount() {



  }

  rowSelected(item){
     
     if(item.index == 1){

      Navigation.push(this.props.componentId,{ 
    component: { 
    name: 'SecondScreen',
    //passProps:{item},
    options: { 
           animations: {
          push: {
      content: {
        y: {
          from:width,
          to: 0,
          duration: 300
        }
      }
    }
         },


         bottomTabs:
      {
        visible:false,
        drawBehind:true,
        animate:true
      },
      animations: {
        pop: {
          enable: true
        }
      },
  
        
          topBar: {
            visible: false,
          animate: true, // Controls whether TopBar visibility changes should be animated
          hideOnScroll: false,
          leftButtonColor: 'white',
          rightButtonColor: 'white',
          drawBehind: false,
          backButton: {
                    color:Constants.textcolor=='black'?'black':'white',

                     },
         
          background: {
           color:Constants.textcolor=='transparent'?'transparent':'transparent',
           },
          }, 

        }
         },
          });

     }

     if(item.index == 2){
      
     }
     if(item.index == 3){
      
     }
     if(item.index == 4){
      
     }
     if(item.index == 5){
      
     }
     if(item.index == 6){
      
     }

  }

  profileAction(){
    Alert.alert("Under Development!")
  }



  render(){
  
    return(
       <ImageBackground source={bgImage} style={{flex: 1, resizeMode: "cover", justifyContent: "center"}}>
      <SafeAreaView style={{flex:1}}>
     <Header navigator={this.props.componentId} colorBackground={true} filterSearch={this.profileAction} showFilter={true} showBackbutton={false}  title=''/>
      
      <View style={styles.outerContainer}>

        <View style={{flex:0.17,alignItems:'center',justifyContent:'center'}}>

        <View style={{flex:0.7,alignItems:'flex-end',justifyContent:'center'}}>
         <Image style={{height:70,width:70}} source={logo} />
        </View>

        <View style={{flex:0.3,alignItems:'center',justifyContent:'center', marginTop:25}}>
         <Text style={{fontSize:18,textAlign:'left'}}>  BADRE ALAM TRADERS </Text>
        </View>
        
        </View>
        
        <View style={{flex:0.1,alignItems:'center', justifyContent:'center'}}>
        <Text style={{fontSize:14,fontFamily:'Arial',textAlign:'center',color:'#666666',margin:10}}> SELECT AN OPTION  </Text>
          </View>

          <View style={{flex:1,justifyContent:'center'}}>
         <FlatList
              data={this.state.finalArray}
              numColumns={2}
              renderItem={({item}) => 
             
              <View style={{flex:1, marginLeft:40,marginRight:40, alignItems:'center',justifyContent:'center', marginTop:10}}>

              <TouchableOpacity style={{height:110,marginRight:10,backgroundColor:'white', borderRadius:20,width:110,margin:5,marginLeft:10}} 
              onPress={()=>this.rowSelected(item)}>
               {/*<ImageBackground source={bgImage} imageStyle={{borderRadius:20}} style={{flex: 1, resizeMode: "cover", justifyContent: "center"}}>
               */}<View style={{flex:1, marginTop:0}}>

              <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
              <Image style={{height:55,width:55, resizeMode:'contain'}} source={item.image} />
              </View>
            
              
              </View>
            </TouchableOpacity>  

            <View style={{flex:0.4, alignItems:'center',justifyContent:'center'}}>
              <Text style={{color:'#333333',marginTop:0,
                     textAlign:'center',fontSize:18, fontFamily:'Arial'}}>
              {item.title}</Text>
              </View>

            </View>
            
            }
              />
              </View>
     
      </View>
      </SafeAreaView>
      </ImageBackground>
      

    )
  }

}


const styles=StyleSheet.create({
   root: {
    
  },
 
    outerContainer:{
      flex:1,
      justifyContent:'center',
      //alignItems:'center'
      //marginBottom:100,
    },
    listView:{
      // paddingRight:20,
      // paddingLeft:20,
      flex:1,

    },
    textView:{
     // height:80,
     flex:9,
      alignItems:'center',
      justifyContent:'flex-end',
      paddingLeft:15,
      paddingRight:15,
      flexDirection:'row',
      // backgroundColor:'green',
    },
    lineView:{
      height:1,
      marginRight:15,
      marginLeft:15,
      backgroundColor:'#E5E5E5',
    },
    textStyle:{
      textAlign:'right',
      color:'#000000',
      fontFamily:'Jameel Noori Nastaleeq',
      fontSize:20,
    },
   

})


module.exports=HomeScreen;
