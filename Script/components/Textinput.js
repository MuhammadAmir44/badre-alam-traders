/*Custom TextInput*/
import React from 'react';
import { View, TextInput } from 'react-native';
const Textinput = props => {
  return (
   
      <TextInput
        underlineColorAndroid="grey"
        placeholder={props.placeholder}
        //placeholderTextColor="#007FFF"
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        returnKeyType={props.returnKeyType}
        numberOfLines={props.numberOfLines}
        multiline={props.multiline}
        onSubmitEditing={props.onSubmitEditing}
        style={props.style}
        blurOnSubmit={false}
        value={props.value}
      />
  
  );
};
export default Textinput;